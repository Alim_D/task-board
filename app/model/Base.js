Ext.define('TB.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'TB.model'
    }
});
