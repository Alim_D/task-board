Ext.define("TB.model.Issues", {
  extend: "TB.model.Base",

  fields: [{
      name: "issueId",
      type: "string",
    }, {
      name: "issueName",
      type: "string",
    }, {
      name: "name",
      type: "string",
    },{
      name: "surname",
      type: "string",
    }, {
      name: "status",
      type: "string",
    }, {
      name: "issueDate",
      type: "date",
    }, {
      name: "isTemp",
      type: "boolean",
      defaultValue: false,
    }]
});
