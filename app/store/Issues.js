Ext.define('TB.store.Issues', {
    extend: 'Ext.data.Store',

    alias: 'store.issues',

    model: 'TB.model.Issues',

    proxy: {
        type: 'rest',
        url: '/api/issues',
        reader: {
            type: 'json',
            rootProperty: ''
        }
    }
});
