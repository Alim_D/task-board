Ext.define('TB.helpers.Utils', {
    singleton: true,
    alternateClassName: 'Utils',
    /**
     * Create dummy data
     */
    createDummyData(length) {
        return Array.from({length: length}, x => {
            return {
                issueId: this.getRandomIssueNumber(),
                issueName: this.getRandomIssueName(),
                name: this.getRandomUserName(),
                surname: this.getRandomUserSurname(),
                status: this.getRandomStatus(),
                priority: this.getRandomPriority(),
                issueDate: this.getRandomDate()
            }
        });
    },

    getRandomIssueNumber() {
        return `TSK-${Math.floor(Math.random() * (9999 - 1000) + 1000)}`;
    },

    getRandomIssueName() {
        const issueNames = ['Bug-fix', 'Improvement', 'Refactor', 'Feature', 'Tech-Deb'];
        return issueNames[Math.floor( Math.random() * issueNames.length)];
    },

    getRandomUserName() {
        const names = ['Иван', 'Петр', 'Сергей', 'Максим', 'Андрей', 'Данил'];
        return names[Math.floor( Math.random() * names.length)];
    },

    getRandomUserSurname() {
        const surnames = ['Иванов', 'Петров', 'Смирнов', 'Кузнецов', 'Попов', 'Данилов'];
        return surnames[Math.floor( Math.random() * surnames.length)];
    },

    getRandomStatus() {
        const statuses = ['plan', 'inProgress', 'testing', 'done'];
        return statuses[Math.floor( Math.random() * statuses.length)];
    },

    getRandomPriority() {
        const priorities = ['must', 'should', 'could'];
        return priorities[Math.floor( Math.random() * priorities.length)];
    },

    getRandomDate() {
        const start = new Date(2019, 0, 1);
        const end = new Date();
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    },
});