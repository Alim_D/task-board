Ext.define('TB.view.board.issue.Issue',  {
    extend: 'Ext.view.View',
    alias: 'widget.issue',
    tpl: [
        '<tpl for=".">',
            '<div class="issue {priority}-issue <tpl if="isTemp">temp-container</tpl>">',
                '<h3>{issueId}</h4>',
                '<p>{issueName}</p>',
            '</div>',
        '</tpl>'
    ],
    itemSelector: 'div.issue',
    selectedItemCls: 'selected-issue',
    listeners: {
        render: 'onIssueRender',
        selectionchange: 'onSelectionChange',
        beforeselect: 'onBeforeIssueSelect',        
    }
});