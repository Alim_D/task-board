Ext.define('TB.view.info.board.Info',  {
    extend: 'Ext.panel.Panel',
    alias: 'widget.infoPanel',
    title: 'Selected Task',
    closable: true,
    closeAction: 'hide',
    hidden: true,
    bind: {
        hidden: '{isInfoPanelHidden}'
    },

    reference: 'infoPanel',
    flex: 0.4,

    listeners: {
        close: 'onInfoPanelClose'
    },

    items: [{
        xtype: 'container',
        layout: 'hbox',
        margin: '10px 0 0 10px',
        items: [{
            xtype: 'container',
            flex: 0.1,
            defaults: {
                labelWidth: 120,
                labelAlign: 'top',
                labelCls: 'issue-info__label',
                fieldCls: 'issue-info__field'
            },
            defaultType: 'displayfield',
            layout: 'vbox',
            items: [{
                fieldLabel: 'Номер задачи',
                bind: '{rec.issueId}',
            }, {
                fieldLabel: 'Название задачи',
                bind: '{rec.issueName}',
            }, {
                fieldLabel: 'Пользователь',
                bind: '{fullName}',
            }]
        }, {
            xtype: 'container',
            flex: 0.3,
            defaults: {
                labelWidth: 120,
                labelAlign: 'top',
                labelCls: 'issue-info__label',
                fieldCls: 'issue-info__field'
            },
            defaultType: 'displayfield',
            layout: 'vbox',
            items: [{
                fieldLabel: 'Статус задачи',
                bind: '{rec.status}',
            }, {
                fieldLabel: 'Важность задачи',
                bind: '{rec.priority}'
            }, {
                fieldLabel: 'Дата',
                bind: '{formatedDate}'
            }]
        }]
    }]
});