Ext.define('TB.view.board.BoardController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.board',

    onInfoPanelClose() {
        this.getViewModel().set('rec', {});
    },

   /**
    * Устанавливается выбранный record во ViewModel,
    * после чего отобразится панель с данными
    * @param {*} view 
    * @param {*} selected 
    */
    onSelectionChange(view, selected) {
        const isSelected = selected.length;
        
        if(isSelected) {
            const data = selected[0].getData();            
            this.getViewModel().set('rec', data);
        } else {
            this.getViewModel().set('rec', {});
        }
    },
    /**
     * Убирает все select-ы
     */
    onBeforeIssueSelect() {
        const childs = this.lookup('board').query('dataview');
        
        Ext.Array.each(childs, item => {
            item.setSelection(null);
        });
        
        return true;
    },

    onIssueRender(view) {
        this.initDD(view);
    },
    /**
     * Инициализируется Drag&Drop для @param cmp
     * @param {*} cmp - component
     */
    initDD(cmp) {
        let isDroped = false;
        // const store = this.get

        new Ext.dd.DragZone(cmp.getEl(), {
            getDragData(e) {
                e.preventDefault();

                const sourceEl = e.getTarget(cmp.itemSelector);
                
                if (sourceEl) {
                    d = sourceEl.cloneNode(true);
                    d.id = Ext.id();
                    return {
                        ddel: d,
                        sourceEl: sourceEl,
                        sourceZone: cmp,
                        sourceStore: cmp.store,
                        repairXY: Ext.fly(sourceEl).getXY(),
                        draggedRecord: cmp.getRecord(sourceEl)
                    }
                }
            },
            getRepairXY() {
                return this.dragData.repairXY;
            }
        });
        
        new Ext.dd.DropZone(cmp.getEl(), {
            getAllowed(source, e) {
                const targetEl = e.getTarget();
                const targetCmp = Ext.Component.from(targetEl);
                const allowed = source.dragData.sourceZone !== targetCmp;
                const proto = Ext.dd.DropZone.prototype;
                return allowed ? proto.dropAllowed : proto.dropNotAllowed;
            },

            notifyOver(source, e) {
                return this.getAllowed(source, e);
            },

           /**
            * Вызывается когда берется компонент для перетаскивания
            * @param {*} source 
            * @param {*} e 
            * @param {*} data 
            */
            notifyEnter(source, e, data) {
                data.draggedRecord.set({ 
                    isTemp: true,
                    status: cmp._issueStatus
                });

                return this.getAllowed(source, e);
            },


           /**
            * Вызывается когда курсор выходит за рамки контейнера для drag-а
            * @param {*} source 
            * @param {*} e 
            * @param {*} data 
            */
            notifyOut(source, e, data) {
                if (!isDroped) {                    
                    data.draggedRecord.set({
                        isTemp: false,
                        status: source.dragData.sourceZone._issueStatus
                    });
                } else {
                    isDroped = false;
                }
            },

           /**
            * Вызывается когда компонент drop-ается, и устанавливается нужный статус
            * @param {*} source 
            * @param {*} e 
            * @param {*} data 
            */
            onContainerDrop(source, e, data) {
                source.dragData.draggedRecord.set({
                    isTemp: false,
                    status: cmp._issueStatus
                });

                isDroped = true;

                return true;
            }
        });
    }
});