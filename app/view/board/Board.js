Ext.define('TB.view.board.Board',  {
    extend: 'Ext.panel.Panel',
    alias: 'widget.board',

    title: 'TASKS BOARD',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    flex: 1,
    viewModel: 'board',
    controller: 'board',

    items: [{
        xtype: 'container',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        reference: 'board',
        
        flex: 1,
        padding: 10,
        defaultType: 'panel',
        defaults: {
            frame: true,
            bodyPadding: 10,
            flex: 1,
            margin: '0 10 0 0',
            scrollable: 'y'
        },
    
        items: [{
                title: 'PLAN',
                layout: 'fit',
                items: [{
                    xtype: 'issue',
                    _issueStatus: 'plan',
                    bind: {
                        store: '{plan}'
                    }
                }],
                reference: 'planColumn'
            }, {
                title: 'IN PROGRESS',
                layout: 'fit',
                items: [{
                    xtype: 'issue',
                    _issueStatus: 'inProgress',
                    bind: {
                        store: '{inProgress}'
                    }
                }],
                reference: 'progressColumn',
            }, {
                title: 'TEST',
                layout: 'fit',
                items: [{
                    xtype: 'issue',
                    _issueStatus: 'testing',
                    bind: {
                        store: '{test}'
                    }
                }],
                reference: 'testColumn'
            }, {
                title: 'DONE',
                layout: 'fit',
                margin: 0,
                items: [{
                    xtype: 'issue',
                    _issueStatus: 'done',
                    bind: {
                        store: '{done}'
                    }
                }],
                reference: 'doneColumn'
            }
        ]
    }, {
        xtype: 'infoPanel'
    }]
});