Ext.define('TB.view.board.BoardModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.board',

    formulas: {
        isInfoPanelHidden: {
            bind: {
                bindTo: '{rec}',
                deep: true
            }, 
            get(rec) {
                return Ext.Object.isEmpty(rec);
            }
        },
        fullName: {
            bind: {
                bindTo: '{rec}',
                deep: true
            }, 
            get(rec) {
                return `${rec.surname} ${rec.name}`;
            }
        },
        formatedDate: {
            bind: {
                bindTo: '{rec}',
                deep: true
            }, 
            get(rec) {
                return Ext.util.Format.date(rec.issueDate, 'd.m.Y');
            }
        }
    },

    stores: {
        issues: {
            type: 'issues',
            autoLoad: true,
            pageSize: null,
            sorters: [
                {
                property: 'name',
                direction: 'ASC'
            }, {
                sorterFn: (a, b) => {
                    const aPr = a.get('priority');
                    const bPr = b.get('priority');

                    if(aPr === 'should' && bPr === 'must' || aPr === 'could' && bPr !== 'could') {
                        return 1;
                    }

                    if(aPr === 'must' && bPr !== 'must' || aPr === 'should' && bPr === 'could') {
                        return -1;
                    }                    

                    return 0;
                }
            }]
        },
        plan: {
            source: '{issues}',
            filters: [{
                property: 'status',
                value: 'plan',
                operator: 'eq'
            }]
        },
        inProgress: {
            source: '{issues}',
            filters: [{
                property: 'status',
                value: 'inProgress',
                operator: 'eq'
            }]
        },
        test: {
            source: '{issues}',
            filters: [{
                property: 'status',
                value: 'testing',
                operator: 'eq'
            }]
        },
        done: {
            source: '{issues}',
            filters: [{
                property: 'status',
                value: 'done',
                operator: 'eq'
            }]
        }
    },

    data: {
        rec: {}
    }
});