/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define("TB.Application", {
  extend: "Ext.app.Application",

  name: "TB",

  quickTips: false,
  platformConfig: {
    desktop: {
      quickTips: true,
    },
  },

  requires: ["Ext.ux.ajax.SimManager"],

  launch() {
    Ext.ux.ajax.SimManager.init({
      delay: 500,
    }).register({
      "/api/issues": {
        type: "json",
        data: TB.helpers.Utils.createDummyData(20),
      },
    });
  },
});
